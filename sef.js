
var code = `
    function fill(line) {
        const NAME = 0;
        const SURNAME = 1;
        const NATIONALITY = 2;
        const BIRTH_DATE = 3;
        const BIRTH_PLACE = 4;
        const ID = 5;
        const ID_COUNTRY = 6;
        const ID_TYPE = 7;
        const CHECK_IN = 8;
        const CHECK_OUT = 9;
        const RESIDENCE_COUNTRY = 10;
        const RESIDENCE_PLACE = 11;

        const countryMapping = {
            'United Kingdom of Great Britain and Northern Ireland': '223',
            'Netherlands': '124',
            'Bulgaria': '028',
            'Portugal': '128',
            'Ukraine': '204',
            'Italy': '083',
            'Brazil': '026',
            'India': '075',
            'France': '059',
            'Korea (Republic of)': '043',
            'Antigua and Barbuda': '008',
            'Poland': '127',
            'Canada': '033',
            'Germany': '132',
            'Australia': '012',
            'Switzerland': '153',
            'Ireland': '080',
            'United States of America': '055',
            'Spain': '054',
            'Argentina': '011',
            'Austria': '013',
            'Denmark': '047',
            'New Zealand': '117',
            'Singapore': '148',
            'Latvia': '191',
            'Finland': '058',
            'China': '036',
            'Sweden': '152',
            'Belgium': '018',
            'Czech Republic': '214',
            'Hungary': '072',
            'Lithuania': '122',
            'Slovakia': '215',
            'South Africa': '002',
            'Colombia': '039',
            'Luxembourg': '096',
            'Belarus': '181'
        };
        const idMapping = {
            'ID': 'B  ',
            'Passport': 'P  '
        };

        console.log('line>', line);
        const match = line.split('#');
        console.log('line.split(#)>', line.split('#'));

        $('#Conteudo_txtNome').val(match[NAME] + ' ' + match[SURNAME]);
        console.log('Conteudo_txtNome>', Conteudo_txtNome);
        
        $('#Conteudo_txtDataNascimento').val(match[BIRTH_DATE]);
        console.log('Conteudo_txtDataNascimento>', match[BIRTH_DATE]);
        
        $('#Conteudo_txtLocalNascimento').val(match[BIRTH_PLACE]);
        console.log('Conteudo_txtLocalNascimento>', match[BIRTH_PLACE]);
        
        $('#Conteudo_lstNacionalidade').val(countryMapping[match[NATIONALITY]]);
        console.log('Conteudo_lstNacionalidade>', countryMapping[match[NATIONALITY]]);
        
        $('#Conteudo_txtLocalResidenciaOrigem').val(match[RESIDENCE_PLACE]);
        console.log('Conteudo_txtLocalResidenciaOrigem>', match[RESIDENCE_PLACE]);
        
        $('#Conteudo_lstPaisResidencia').val(countryMapping[match[RESIDENCE_COUNTRY]]);
        console.log('Conteudo_lstPaisResidencia>', countryMapping[match[RESIDENCE_COUNTRY]]);
        
        $('#Conteudo_txtNumPassaporteBI').val(match[ID]);
        console.log('Conteudo_txtNumPassaporteBI>', match[ID]);
        
        $('#Conteudo_lstDID').val(idMapping[match[ID_TYPE]]);
        console.log('Conteudo_lstDID>', match[ID_TYPE]);
        
        $('#Conteudo_lstPaisEmissor').val(countryMapping[match[ID_COUNTRY]]);
        console.log('Conteudo_lstPaisEmissor>', countryMapping[match[ID_COUNTRY]]);
        
        $('#Conteudo_txtDataEntrada').val(match[CHECK_IN]);
        console.log('Conteudo_txtDataEntrada>', match[CHECK_IN]);
        
        $('#Conteudo_txtDataSaida').val(match[CHECK_OUT]);
        console.log('Conteudo_txtDataSaida>', match[CHECK_OUT]);
    };

window.fill = fill;
`;

var script = document.createElement('script');
var code = document.createTextNode('(function() {' + code + '})();');
script.appendChild(code);
(document.body || document.head).appendChild(script);