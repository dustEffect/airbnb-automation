apartMapping = {
    'Totalmente Renovado, metro à porta': 'T2',
    'Espaço Renovado a 5 minutos a pé da Ponte D. Luíz': 'T0',
    'T1 Renovado c/ metro à porta': 'T1',
    'Espaço Renovado a 5 minutos a pé da Ponte D. Luíz': 'EA',
    'Loft c/ Varanda Solarenga a 5 Minutos Ponte D Luíz': 'EB',
};

const copyToClipboard = str => {
    const el = document.createElement('textarea');
    el.value = str;
    document.body.appendChild(el);
    el.select();
    document.execCommand('copy');
    document.body.removeChild(el);
};

const userName = document.querySelector('._26piifo').textContent;
const apartName = document.querySelector('._1354udht').textContent;
const apartNickname = apartMapping[apartName];
const contactPhone = document.querySelector('._8b6uza1').textContent.match(/\+[\d| ]+/g)[0];
const contactName = `ZAIRBNB ${userName} ${apartNickname}`;

copyToClipboard(`${contactName},${contactPhone}`);
